# Spring Cloud Configuration Data Repository #

This README would normally document whatever steps are necessary to change the configuration data for your services as well as how these configuration data are well organized in this repository.

### What is this repository for? ###

This is the central repository of configuration data for all the services running in both production and testing environment.

### How are the configuration data organized in this repository ? ###
* Environment

1. Production
1. Testing

* Service Name

1. config-service
1. eureka-service
1. auth-service
1. turbine-service
1. device-service

### How do I change the configuration data for my service? ###

TIPS:

1. All operations can be done via Bitbucket Web UI
1. All operations will tigger notification via e-mail
1. The review process is named "Pull request" which automatically create a temporary branch

STEY BY STEP (with screenshot):

Step 1 : log in and choose the repository
 
![QQ20160219-2@2x.png](https://bitbucket.org/repo/j45zbM/images/2178499060-QQ20160219-2@2x.png)

Step 2: Choose the data file which you are going to modify

![QQ20160219-3@2x.png](https://bitbucket.org/repo/j45zbM/images/1011850101-QQ20160219-3@2x.png)

Step 3: Edit the file

![QQ20160219-4@2x.png](https://bitbucket.org/repo/j45zbM/images/816069020-QQ20160219-4@2x.png)

Step 4: Commit the change

![QQ20160219-5@2x.png](https://bitbucket.org/repo/j45zbM/images/949395878-QQ20160219-5@2x.png)

Step 5: Create a Pull request for this change

![QQ20160219-6@2x.png](https://bitbucket.org/repo/j45zbM/images/1846237163-QQ20160219-6@2x.png)

Step 6 : Choose the reviewers for this Pull request

![QQ20160219-7@2x.png](https://bitbucket.org/repo/j45zbM/images/2669017786-QQ20160219-7@2x.png)

Step 7 : Commit this change

![QQ20160219-8@2x.png](https://bitbucket.org/repo/j45zbM/images/470499611-QQ20160219-8@2x.png)

Step 8 : View your Pull request

![QQ20160219-9@2x.png](https://bitbucket.org/repo/j45zbM/images/1957463253-QQ20160219-9@2x.png)

Step 9 : Reviewer log in and view this Pull request

![QQ20160219-10@2x.png](https://bitbucket.org/repo/j45zbM/images/802578877-QQ20160219-10@2x.png)

Step 10 : Reviewer check and approve this Pull request

![QQ20160219-11@2x.png](https://bitbucket.org/repo/j45zbM/images/4227280370-QQ20160219-11@2x.png)

Step 11 : Reviewer merge this Pull request into major

![QQ20160219-12@2x.png](https://bitbucket.org/repo/j45zbM/images/1607306023-QQ20160219-12@2x.png)

Step 12 : Reviewer submit the merge

![QQ20160219-13@2x.png](https://bitbucket.org/repo/j45zbM/images/3778922706-QQ20160219-13@2x.png)

Step 13 : You can view the historical Pull requests

![QQ20160219-14@2x.png](https://bitbucket.org/repo/j45zbM/images/1171937833-QQ20160219-14@2x.png)


### Resources ###

* [spring cloud config service official site](http://cloud.spring.io/spring-cloud-config/spring-cloud-config.html)
* [spring cloud config github](https://github.com/spring-cloud/spring-cloud-config)